package example0

import org.hamcrest.CoreMatchers
import org.junit.Test

import org.junit.Assert.*

class CalculatorTest {

    @Test
    fun add() {
        // Arrange
        val sut = Calculator()

        // Act
        val result = sut.add(1, 2)

        // Assert
        assertThat(result, CoreMatchers.equalTo(3))
    }
}