package example1.client

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import example1.service.book.BookService
import org.junit.Test


class LendBookManagerTest {

    @Test
    fun whenBookIsAvailable_thenLendMethodIsCalled() {
        val mockBookService : BookService = mock()

        whenever(mockBookService.inStock(100)).thenReturn(true)

        val manager = LendBookManager(mockBookService)

        manager.checkout(100, 1)

        verify(mockBookService).lend(100, 1)

    }

    @Test(expected = IllegalStateException::class)
    fun whenBookIsNotAvailable_thenTrhowIllegalStateException() {
        val mockBookService : BookService = mock()

        whenever(mockBookService.inStock(100)).thenReturn(false)

        val manager = LendBookManager(mockBookService)

        manager.checkout(100, 1)

    }

}